import os
import pygame
import random

# Meteorite
METEORITE = pygame.image.load(os.path.join("Assets", "meteorite.png"))

SIZE = 100


class Meteorite:
    VELOCITY = 10
    SPAWN_POINT_Y = -SIZE - 50

    def __init__(self, y=SPAWN_POINT_Y) -> object:
        self.x = self.get_new_x()
        self.y = y
        self.meteorite_img = METEORITE
        self.meteorite_img = pygame.transform.scale(self.meteorite_img, (SIZE, SIZE))
        self.mask = pygame.mask.from_surface(self.meteorite_img)

    def update(self):
        self.y += self.VELOCITY

    def draw(self, window):
        window.blit(self.meteorite_img, (self.x, self.y))

    def set_x(self, x):
        self.x = x

    def get_y(self):
        return self.y

    def get_x(self):
        return self.x

    def get_new_x(self):
        return random.randint(0, 200)

    def get_width(self):
        return self.meteorite_img.get_width()

    def get_height(self):
        return self.meteorite_img.get_height()
