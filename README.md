# Meteorite Dodger

Ce projet consiste au projet étudiant de 5e session de Nouvelles Technologies de Philippe Desbiens et Guillaume Thivierge. 
C'est un Flappy Bird qui implémente du Machine Learning, les "Birds" sont donc contrôlés par une IA évolutive.

## Démarrage rapide

Ces instructions vous permettront d'obtenir une copie opérationnelle du projet sur votre machine à des fins de développement.

### Prérequis

* [Git](https://git-scm.com/downloads) - Système de contrôle de version. Utilisez la dernière version.
* [PyCharm](https://www.jetbrains.com/pycharm/)  - IDE. Utilisez la dernière version.
* [Python 3.7.9](https://www.python.org/downloads/release/python-379/) - Langage. Veuillez utiliser **spécifiquement cette 
  version.**
* [Pygame](https://www.pygame.org/wiki/GettingStarted#Pygame%20Installation) Librairie de visuels - Utilisez la dernière version.
* [NEAT](https://neat-python.readthedocs.io/en/latest/) Librairie d'IA -  Utilisez la dernière version.

### Compiler une version de développement

Clonez le projet.

Téléchargez les librairies via PyCharm ou l'invite de commande.

```
https://gitlab.com/phil.desbiens/meteorite-dodger.git
```

Ouvrez le projet dans PyCharm.

Lancez le programme.

## Auteurs

* **Philippe Desbiens** - *Programmeur*
* **Guillaume Thivierge** - *Programmeur*

## Remerciements

* Merci à Tech With Tim et son tutoriel [Python Flappy Bird AI Tutorial (with NEAT)](https://www.youtube.com/watch?v=MMxFDaIOHsE&list=PLzMcBGfZo4-lwGZWXz5Qgta_YNX3_vLS2) qui nous a permis d'en apprendre beaucoup sur python et le Machine Learning.
