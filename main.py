import os
import neat
import pygame

from Meteorite import Meteorite
from Ship import Ship

pygame.font.init()

WIDTH, HEIGHT = 300, 400
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("METEORITE DODGER")

# Background
BG = pygame.transform.scale(pygame.image.load(os.path.join("Assets", "background-black.png")), (WIDTH, HEIGHT))


def collide(obj1, obj2):
    offset_x = obj2.x - obj1.x
    offset_y = obj2.y - obj1.y
    return obj1.mask.overlap(obj2.mask, (offset_x, offset_y)) is not None


def main(genomes, config):
    FPS = 60
    nets = []
    ge = []
    ships = []
    meteorites = [Meteorite()]
    add_meteorite = True

    for _, g in genomes:
        net = neat.nn.FeedForwardNetwork.create(g, config)
        nets.append(net)
        ships.append((Ship(100, 350)))
        g.fitness = 0
        ge.append(g)

    clock = pygame.time.Clock()

    def redraw_window():
        WIN.blit(BG, (0, 0))
        for ship in ships:
            ship.draw(WIN)
        for meteorite in meteorites:
            meteorite.draw(WIN)
        pygame.display.update()

    run = True
    while run:
        clock.tick(FPS)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
                quit()
                break

        meteorites_ind = 0
        if len(ships) > 0:
            if len(meteorites) > 1 and ships[0].y > ships[0].y + meteorites[0].y:
                meteorites_ind = 1
        else:
            run = False
            break

        for x, ship in enumerate(ships):

            output = nets[x].activate((ship.x, abs(ship.x - meteorites[meteorites_ind].x - 5),
                                       abs(ship.x - meteorites[meteorites_ind].x + 105)))

            if output[0] >= 0.5 and ship.x + ship.vel + ship.get_width() < WIDTH:
                ge[x].fitness += 0.1
                ship.move_right()
            elif ship.x - ship.vel > 0:
                ge[x].fitness += 0.1
                ship.move_left()

        redraw_window()

        for meteorite in meteorites:
            if meteorite.y > HEIGHT + 100:
                meteorites.remove(meteorite)
                add_meteorite = True
            elif meteorite.y > HEIGHT - HEIGHT / 2 and add_meteorite:
                meteorites.append(Meteorite())
                add_meteorite = False
            else:
                meteorite.update()

            for y, ship in enumerate(ships):
                if collide(meteorite, ship):
                    ge[y].fitness -= 1
                    ships.pop(y)
                    nets.pop(y)
                    ge.pop(y)

                elif ship.x - ship.vel < 1 or ship.x + ship.vel + ship.get_width() > WIDTH - 1:
                    ge[y].fitness -= 0.1

        redraw_window()


def run(config_path):
    config = neat.config.Config(neat.DefaultGenome, neat.DefaultReproduction,
                                neat.DefaultSpeciesSet, neat.DefaultStagnation, config_path)

    p = neat.Population(config)
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)

    winner = p.run(main, 50)


if __name__ == "__main__":
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, "config-feedforward.txt")
    run(config_path)
