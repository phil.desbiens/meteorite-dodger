import pygame
import os

# Player ship
YELLOW_SPACE_SHIP = pygame.image.load(os.path.join("Assets", "pixel_ship_yellow.png"))


class Ship:
    def __init__(self, x, y) -> object:
        self.x = x
        self.y = y
        self.vel = 10
        self.ship_img = YELLOW_SPACE_SHIP
        self.ship_img = pygame.transform.scale(self.ship_img, (33, 30))
        self.mask = pygame.mask.from_surface(self.ship_img)

    def draw(self, window):
        window.blit(self.ship_img, (self.x, self.y))

    def get_width(self):
        return self.ship_img.get_width()

    def get_height(self):
        return self.ship_img.get_height()

    def move_left(self):
        self.x -= self.vel

    def move_right(self):
        self.x += self.vel
